const express = require("express");
const { sequelize, User } = require("./models");

const app = express();
app.use(express.json());

app.post("/users", async (req, res) => {
  const { name, email, role } = req.body;

  try {
    const user = await User.create({ name, email, role });
    return res.json(user);
  } catch (err) {
    console.log("err :>> ", err);
    return res.status(500).json(err);
  }

 
});

app.get("/users",async(req,res)=>{
  try{
    const users = await User.findAll();
    return res.json(users)
  }catch(err){
    return res.status(500).json({error:err.message,message:"something went wrong"});
  }
})

app.get("/users/:uuid",async(req,res)=>{
  const uuid = req.params.uuid;
  try{
    const user = await User.findOne({where:{uuid:uuid}});
    return res.json(user)
  }catch(err){
    return res.status(500).json({error:err.message,message:"something went wrong"});
  }
})

app.listen({port:5000},async(req,res)=>{
  console.log("server online 5000");
  await sequelize.authenticate();
  console.log("data base Conected!");
}) 

// "username": "blinkit_client",
//     "password": "GYZahtpEx8p69RucSm0lNMwV7vcjFFGs",
//     "database": "vaibhav",
//     "host": "dpg-cfh2uu9a6gdvgklv7710-a.oregon-postgres.render.com",
//     "dialect": "postgres",
//     "dialectOptions": {
//       "ssl": {
//         "key": "",
//         "cert": "",
//         "ca": ""
//       }
//     }
